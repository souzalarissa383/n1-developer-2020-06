	// Função para validar campos do form


function validar() {
    var nome = form.nome.value;
    var email = form.email.value;
    var assunto = form.assunto.value;
    var mensagem = form.mensagem.value;

    //Verifica se o campo foi preenchido
    if (nome == "") {
        //mostrar alerta se campo estiver vazio
        alert('Preencha o campo nome!!!!');
        // focar campo 
        form.nome.focus();
        return false;
    }

    // campo precisa ter @ 
    if (email == "" || email.indexOf('@') == -1) {
        //mostrar alerta se campo estiver vazio
        alert('Preencha o campo E-mail!!!');
        // focar campo 
        form.email.focus();
        return false;
    }

    if (assunto == "") {
        alert('Preencha o campo assunto!!!');
        form.assunto.focus();
        return false;
    }
    if (mensagem == "") {
        alert('Preencha campo mensagem!!!');
        form.mensagem.focus();
        return false;
    }
    var nome = document.getElementById("nomeid");
    alert('Obrigado sr(a) ' + nome.value + ' os seus dados foram encaminhados com sucesso');
}